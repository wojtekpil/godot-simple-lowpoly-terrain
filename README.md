**Basic and simple lowpoly terrain generator plugin for godot**

![Screen](https://bitbucket.org/wojtekpil/godot-simple-lowpoly-terrain/raw/226ef5c4b3db6babc3fc88881aee4a88b6268fc8/screen.jpg)


Simple realtime editor plugin for lowpoly terrain. Inspired by this video https://www.youtube.com/watch?v=mGCwjvAibyw by codat. This plugin allows to visualise a created terrain without lunching the game.
For simple low poly scenes mainly.

*  Supports procedural and hightmap based generation
*  No LOD support
*  No chunk system
*  Mesh generation on CPU for easy physics interactions (should be good enough for simple lowpoly scenes)